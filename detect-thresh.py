#!/usr/bin/env python2
from PIL import ImageChops
from pyscreenshot import grab
from time import sleep
import json
import pyautogui

# Load config
with open('config.json') as c:
  conf = json.load(c)

# Calculate box coordinates from x, y and radius
boxes = []
for g in conf['grabs']:
  boxes.append( (g[0]-g[2], g[1]-g[2], g[0]+g[2], g[1]+g[2]) )

# Grab starting images to compare
source = []
for i, b in enumerate(boxes):
  source.append(grab(bbox=b).convert('RGB'))
  #source[i].show() # Useful for debugging box locations

# Main loop
while True:
  dest   = []

  sleep(1)
  for i, b in enumerate(boxes):
    dest.append(grab(bbox=b).convert('RGB'))
    #dest.append(ImageChops.difference(tmp, source[i]))
    #diff.append(dest[i].getbbox())

  # Write output if diffs are detected
  for i, d in enumerate(dest):
    diff = ImageChops.difference(source[i], dest[i])
    hist = diff.histogram()
    dico = len(filter(lambda a: a != 0, hist))

    print str(i) + ":" + str(dico)
    if dico > 100:
      pyautogui.click(conf['outputloc'][0], conf['outputloc'][1])
      pyautogui.typewrite(conf['outputtext'])
      print 'I have done the deed!'
      exit(0)

