#!/usr/bin/evn python2
# This is a small testing utility to help figure out where to grab

from pyscreenshot import grab

loc = [434, 826]
r   = 20
im = grab(bbox=(loc[0]-r, loc[1]-r, loc[0]+r, loc[1]+r))
im.show()

